public class Parser{
    String eingabe;
    int pos, max;
    Parser (String _eingabe){
        eingabe = _eingabe;
        pos = 0;
        max = eingabe.length();
    }

    public boolean parse() {
        if(pruefeS()){
            return true;
        } else return false;
    }

    //Regel S -> Ausdruck
    public boolean pruefeS(){
        if(!eingabe.isEmpty()){
            return this.pruefeA();
        } else return false;
    }

    //Regel Ausdruck -> Variable | Variable + Ausdruck
    public boolean pruefeA(){
        // -> Variable
        if(max - pos == 1){
            return pruefeV(eingabe.charAt(pos));
        } 
        // -> Variable + Ausdruck
        else if(max - pos > 1 && this.pruefeV(eingabe.charAt(pos))){
            pos++;
            if(eingabe.charAt(pos) == '+'){
                pos++;
                return this.pruefeA();
            } else return false;
        } else return false;
    }

    //Regel Variable -> x | y | z
    public boolean pruefeV(char var){
        if(!eingabe.isEmpty() && (var == 'x' || var == 'y' || var == 'z')){
            return true;
        } else return false;
    }
}