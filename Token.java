

public class Token
{
    String terminal, typ;
    public Token(String pTyp, String pTerminal)
    {
        typ = pTyp;
        terminal = pTerminal;
    }
    
    public String getTyp(){
        return typ;
    }
    
    public String getTerminal(){
        return terminal;
    }

}
